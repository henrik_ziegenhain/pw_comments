config.tx_extbase.persistence.classes {
    PwCommentsTeam\PwComments\Domain\Model\FrontendUser {
        mapping {
            tableName = fe_users
        }
    }
}